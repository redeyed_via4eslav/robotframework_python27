*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Resources1.robot
Test Setup  Start Browser and Maximize
Test Teardown  Close Browser Window

*** Variables ***
    ${Browser}  Chrome
    ${Url}  http://thetestingworld.com/testings


*** Test Cases ***
#Robot First Test Case
#    Enter Credentinals  test  test  test

Robot Second Test Case
    Select Radio Button  add_type  office
    Create folder at Runtime

*** Keywords ***
Enter Credentinals
    [Arguments]  ${Username}  ${Email}  ${Password}
    Input Text  name:fld_username  ${Username}
    Input Text  name:fld_email  ${Email}
    Input Text  name:fld_password  ${Password}

