*** Settings ***
Library  SeleniumLibrary
#Resource  ../Resources/Resources1.robot
#
*** Variables ***
#    ${Browser}  Chrome
#    ${Url}  https://robotframework.org/

*** Test Cases ***
Switch Tab
    Open Browser  https://robotframework.org/  Chrome
    Click Element  //a[text()='robotframework.org/rpa']
    Select Window  Robot Framework
    ${url1}=  Get Location
    log To Console  ${url1}
    Select Window  Robot Framework RPA
    ${url2}=  Get Location
    Log To Console  ${url2}


*** Keywords ***
Enter Credentinals
    [Arguments]  ${Username}  ${Email}  ${Password}
    Input Text  name:fld_username  ${Username}
    Input Text  name:fld_email  ${Email}
    Input Text  name:fld_password  ${Password}